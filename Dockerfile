FROM ruby:2.6-alpine

RUN apk add --no-cache build-base libxml2-dev libxslt-dev
RUN apk add --no-cache sqlite-dev
RUN apk add --no-cache tzdata
RUN apk add --no-cache nodejs
