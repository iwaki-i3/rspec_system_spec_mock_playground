Rails.application.routes.draw do
  resource :session, only: [:show, :create, :destroy]
  resource :user, only: [:show] do
    get :delayed, to: 'users#show_delayed'
  end

  namespace :admin do
    resources :users, only: [:index, :new, :create]
  end
end
