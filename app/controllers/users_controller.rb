class UsersController < ApplicationController
  # GET /user
  def show
  end

  def show_delayed
    @user = current_user_id.presence.try do |id|
      User.find(id)
    end
  end
end
