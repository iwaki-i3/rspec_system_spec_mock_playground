class SessionsController < ApplicationController
  def show
    @logged_in = current_user_id.present?
    @login_form = LoginForm.new
  end

  def create
    @login_form = LoginForm.new(params.require(:login_form).permit(:name))
    if @login_form.login
      session[:user_id] = @login_form.login_user_id
      redirect_to user_path
    else
      render :show
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to action: :show
  end
end
