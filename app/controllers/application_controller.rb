class ApplicationController < ActionController::Base
  private def current_user_id
    session[:user_id]
  end
end
