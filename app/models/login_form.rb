class LoginForm
  include ActiveModel::Model

  attr_accessor :name
  attr_reader :login_user_id

  def login
    user = User.find_by(name: name)
    if user.present?
      @login_user_id = user.id
      true
    else
      errors.add(:name, 'is invalid')
      false
    end
  end
end
